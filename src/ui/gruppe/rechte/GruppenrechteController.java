package ui.gruppe.rechte;

import gruppe.Gruppe;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import ui.FiBuController;

import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.ResourceBundle;

public class GruppenrechteController implements Initializable {

    private String currentItemKonten;
    private String currentItemGroup;

    @FXML
    private ComboBox<String> group;
    @FXML
    private ListView<String> groupKonten;
    @FXML
    private ListView<String> konten;
    @FXML
    private CheckBox abschließen;
    @FXML
    private CheckBox usermanagement;
    @FXML
    private CheckBox stornieren;


    /**
     * Initialisiert Daten
     *
     * @param location
     * @param resources
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        fillCBGroup();
        group.setValue(group.getItems().get(0));
        fillLists();

        konten.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {

                currentItemKonten = newValue;
            }
        });

        groupKonten.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {

                currentItemGroup = newValue;
            }
        });

    }

    /**
     * Füllt die Listen
     */
    public void fillLists() {
        ArrayList<String> konten      = FiBuController.getKonten().getKontenAsList();
        ArrayList<String> groupKonten = FiBuController.getGroups().getGruppe(group.getValue()).getGruppenrechte().getKonten();


        groupKonten.forEach(groupKonto -> {
            konten.remove(groupKonto);
        });

        ObservableList<String> itemsKonten = FXCollections.observableArrayList(
                konten);

        ObservableList<String> itemsGroup = FXCollections.observableArrayList(
                groupKonten);


        this.konten.setItems(itemsKonten);
        this.groupKonten.setItems(itemsGroup);




    }

    /**
     * Füllt die Gruppen-ComboBox
     */
    private void fillCBGroup() {

        ObservableList<String> items = FXCollections.observableArrayList(
                FiBuController.getGroups().getGroupsAsList());

        group.setItems(items);

    }

    /**
     * Bei einem Doppelklick Wird das Konto der anderen Liste hinzugefügt
     *
     * @param mouseEvent
     */
    public void moveKonto(MouseEvent mouseEvent) {

        if (mouseEvent.getClickCount() == 2) {
            switch (((ListView) mouseEvent.getSource()).getId()) {
                case "groupKonten":
                    if (!currentItemGroup.equals("")) {
                        konten.getItems().add(currentItemGroup);
                        groupKonten.getItems().remove(currentItemGroup);
                        currentItemGroup = groupKonten.getSelectionModel().getSelectedItem();
                    }

                    break;
                case "konten":
                    if (!currentItemKonten.equals("")) {
                        groupKonten.getItems().add(currentItemKonten);
                        konten.getItems().remove(currentItemKonten);
                        currentItemKonten = konten.getSelectionModel().getSelectedItem();
                        ;
                    }
                    break;
            }

        }
    }

    /**
     * Speichert die Gruppenrechte
     */
    public void save() {

        for (Object group : group.getItems()) {


            if (groupKonten.getItems() != null) {

                for (Object groupKonto : groupKonten.getItems()) {

                    FiBuController.getGroups().getGruppe((String) group).getGruppenrechte().addKontoRecht((String) groupKonto);
                }
            }


        }

        FiBuController.getGroups().saveGroups("gruppen");

    }


}
