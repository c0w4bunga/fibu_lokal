package ui.user;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.stage.Stage;
import ui.FiBu;
import ui.FiBuController;
import user.User;

import java.net.URL;
import java.util.ResourceBundle;

public class UserController implements Initializable {

    private String currentUsername;

    @FXML
    private ListView<String> nutzerListe;
    @FXML
    private TextField loginname;
    @FXML
    private PasswordField password;
    @FXML
    private TextField username;


    /**
     * Initialisiert Daten
     *
     * @param location
     * @param resources
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        fillList();
        nutzerListe.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (newValue != null) {
                    fillUserData(newValue);
                    currentUsername = newValue;
                }
            }
        });

    }

    /**
     * Füllt die Nutzerliste
     */
    private void fillList() {
        ObservableList<String> items = FXCollections.observableArrayList(
                FiBuController.getUsers().getUsersAsList());

        this.nutzerListe.setItems(items);
    }

    /**
     * Aktuallisiert die Textfelder
     *
     * @param username
     */
    private void fillUserData(String username) {
        User user = FiBuController.getUsers().getUser(username);
        loginname.setText(user.getLoginname());
        password.setText(user.getPassword());
        this.username.setText(user.getName());

    }


    /**
     * Legt einen neuen Nutzer an
     */
    public void addUser() {

        Alert alert = new Alert(Alert.AlertType.ERROR);

        if (FiBuController.getUsers().getUser(username.getText()) == null) {
            if (!loginname.getText().equals("")
                    && !username.getText().equals("")
                    && !password.getText().equals("")) {

                FiBuController.getUsers().addUser(new User(loginname.getText(), username.getText(), password.getText()));
                fillList();
                currentUsername = username.getText();
            } else {
                alert.setTitle("Pflichtfelder");
                alert.setHeaderText("Bitte alle Felder ausfüllen");
                alert.showAndWait();
            }
        } else {
            alert.setAlertType(Alert.AlertType.INFORMATION);
            alert.setHeaderText("Nutzer exestiert bereits!");
            alert.showAndWait();
        }
    }

    /**
     * Löscht den aktuell ausgewählten Nutzer
     */
    public void deleteUser() {

        FiBuController.getUsers().deleteUser(currentUsername);
        fillList();

    }

    /**
     * Speichert die aktuellen Nutzerwerte
     */
    public void save() {

        FiBuController.getUsers().saveUsers("nutzer");
        fillList();

    }

    /**
     * Schließt das Nutzerfenster
     */
    public void close() {

        ((Stage) nutzerListe.getScene().getWindow()).close();

    }
}
