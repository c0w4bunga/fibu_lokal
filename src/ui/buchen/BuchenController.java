package ui.buchen;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.*;
import javafx.stage.Stage;
import konto.Konto;
import ui.FiBuController;


import java.net.URL;
import java.util.*;

public class BuchenController implements Initializable {

    public static String CURRENT_ITEM;
    private String currentTable;
    private Konto currentItemTableSoll;
    private Konto currentItemTableHaben;

    @FXML
    private ListView<String> kontenListe;
    @FXML
    private TableView tableSoll;
    @FXML
    private TableView tableHaben;
    @FXML
    private TableColumn<Konto, String> kontenColumnSoll;
    @FXML
    private TableColumn<Konto, String> werteColumnSoll;
    @FXML
    private TableColumn<Konto, String> kontenColumnHaben;
    @FXML
    private TableColumn<Konto, String> werteColumnHaben;


    /**
     * Initialisiert Daten
     *
     * @param location
     * @param resources
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        fillList();
        kontenColumnSoll.setCellValueFactory(new PropertyValueFactory<Konto, String>("name"));
        werteColumnSoll.setCellValueFactory(new PropertyValueFactory<Konto, String>("currentBuchungswert"));
        kontenColumnHaben.setCellValueFactory(new PropertyValueFactory<Konto, String>("name"));
        werteColumnHaben.setCellValueFactory(new PropertyValueFactory<Konto, String>("currentBuchungswert"));

        kontenListe.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {

                CURRENT_ITEM = newValue;
            }
        });

        tableSoll.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Konto>() {
            @Override
            public void changed(ObservableValue<? extends Konto> observable, Konto oldValue, Konto newValue) {

                currentItemTableSoll = newValue;
            }
        });

        tableHaben.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Konto>() {
            @Override
            public void changed(ObservableValue<? extends Konto> observable, Konto oldValue, Konto newValue) {

                currentItemTableHaben = newValue;
            }
        });

    }

    /**
     * Füllt die Kontenliste
     */
    private void fillList() {

        ObservableList<String> items = FXCollections.observableArrayList(
                FiBuController.getKonten().getKontenAsList());

        this.kontenListe.setItems(items);


    }


    /**
     * Gibt das entsprechende konto zurück
     *
     * @param name
     * @return Konto
     */
    private Konto getKonto(String name) {

        for (Object konto : kontenListe.getItems()) {

            if (name.equals((String) konto)) {
                return FiBuController.getKonten().getKonto(name);
            }

        }
        return null;
    }

    /**
     * Fügt das aktuelle Item der entsprechenden Tabelle hinzu
     *
     * @return boolean
     */
    private boolean addItemtoTable() {

        Konto konto = getKonto(CURRENT_ITEM);

        if (currentTable.equals("tableSoll")) {
            return tableSoll.getItems().add(konto);
        } else if (currentTable.equals("tableHaben")) {
            return tableHaben.getItems().add(konto);
        }

        return false;
    }


    /**
     * Öffnet ein Dialog zum Eingeben des Buchungswertes
     *
     * @return boolean
     */
    private boolean runBuchungsWert() {

        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("Setze Buchungswert");
        dialog.setHeaderText("Setze den Buchungswert");
        dialog.setContentText("Buchungswert:");

        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {
            FiBuController.getKonten().getKonto(BuchenController.CURRENT_ITEM).setCurrentBuchungswert(Double.parseDouble(result.get()));
            return true;
        } else {
            clearTables();
        }
        return false;
    }

    /**
     * Überprüft ob die Summe der Sollseite und die Summe der Habenseite übereinstimmen
     *
     * @return boolean
     */
    private boolean checkBuchungsSumme() {

        double soll = 0;
        double haben = 0;
        for (Object item : tableSoll.getItems()) {
            soll += ((Konto) item).getCurrentBuchungswert();
        }

        for (Object item : tableHaben.getItems()) {
            haben += ((Konto) item).getCurrentBuchungswert();
        }

        if (soll == haben && soll != 0 && haben != 0)
            return true;

        return false;
    }

    /**
     * Leert die Tabellen und füllt die Kontenliste
     */
    private void clearTables() {

        tableSoll.getItems().clear();
        tableHaben.getItems().clear();
        fillList();

    }

    /**
     * Führt die Buchung aus
     */
    public void buchen() {
        Map<String, Double> sollKonten = new HashMap<>();
        Map<String, Double> habenKonen = new HashMap<>();
        if (checkBuchungsSumme()) {
            for (Object item : tableSoll.getItems()) {
                sollKonten.put(((Konto) item).getName(), ((Konto) item).getCurrentBuchungswert());
            }

            for (Object item : tableHaben.getItems()) {
                ((Konto) item).setCurrentBuchungswert((-((Konto) item).getCurrentBuchungswert()));
                habenKonen.put(((Konto) item).getName(), ((Konto) item).getCurrentBuchungswert());
            }

            FiBuController.getBuchungen().addBuchung(sollKonten, habenKonen);
        }

        clearTables();
        FiBuController.getBuchungen().saveBuchungen("buchungen");

    }


    /**
     * Erkennt den Drag und startet den drag und drop Vorgang
     */
    public void dragDetect() {
        Dragboard db = kontenListe.startDragAndDrop(TransferMode.MOVE);
        ClipboardContent content = new ClipboardContent();
        content.putString(kontenListe.getId());
        db.setContent(content);

    }

    /**
     * Beim betreten der Solltabelle wird currentTable aktualisiert
     */
    public void dragEnterSoll() {
        currentTable = "tableSoll";
    }

    /**
     * Beim betreten der Habentabelle wird currentTable aktualisiert
     */
    public void dragEnterHaben() {
        currentTable = "tableHaben";
    }

    /**
     * Akzeptiert den drag und drop Vorgang
     *
     * @param event
     */
    public void dragOver(DragEvent event) {
        if (event.getDragboard().hasString())
            event.acceptTransferModes(TransferMode.MOVE);


    }

    /**
     * Beim droppen des Items wir dieser der Tabelle hinzugefügt
     * und es wird nach einem Buchungswert abgefragt
     *
     * @param event
     */
    public void dragDropped(DragEvent event) {
        Dragboard db = event.getDragboard();
        if (db.hasString()) {
            if (db.getString().equals("kontenListe")) {
                if (runBuchungsWert() && addItemtoTable()) {
                    kontenListe.getItems().remove(CURRENT_ITEM);
                    CURRENT_ITEM = "";
                }


            }
        }

    }

    /**
     * Bei einem Doppelklick auf ein Item einer Tabelle wird das Item entfernt
     *
     * @param mouseEvent
     */
    public void removeKontoFromTable(MouseEvent mouseEvent) {

        if (mouseEvent.getClickCount() == 2) {
            switch (((TableView) mouseEvent.getSource()).getId()) {
                case "tableSoll":
                    kontenListe.getItems().add(currentItemTableSoll.getName());
                    tableSoll.getItems().remove(currentItemTableSoll);
                    break;
                case "tableHaben":
                    kontenListe.getItems().add(currentItemTableHaben.getName());
                    tableHaben.getItems().remove(currentItemTableHaben);
                    break;
            }

        }
    }


    /**
     * Schließt das Buchungsfenster
     */
    public void close() {

        ((Stage) tableSoll.getScene().getWindow()).close();

    }


}
