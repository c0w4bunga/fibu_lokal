package ui;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.Pair;
import user.Users;

import java.util.Optional;


public class FiBu extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Startet das FiBu Fenster
     *
     //* @param primaryStage
     */
    @Override
    public void start(Stage primaryStage) {

        Alert alert = new Alert(Alert.AlertType.ERROR);

        Optional<Pair<String, String>> result = login();

        result.ifPresent(usernamePassword -> {
            try {
                boolean validate = Users.loadUsers("nutzer").validateUser(usernamePassword.getKey(),usernamePassword.getValue());
                if (validate) {
                    Parent root = FXMLLoader.load(getClass().getResource("FiBu.fxml"));
                    primaryStage.setTitle("FiBu");
                    primaryStage.setScene(new Scene(root));
                    primaryStage.setResizable(false);
                    primaryStage.show();
                } else {

                    alert.setTitle("Login");
                    alert.setHeaderText("Falscher Loginname oder Passwort");
                    alert.showAndWait();
                    start(primaryStage);

                }
            } catch (Exception e) {
                System.out.println("not found");
                alert.setTitle("Error");
                alert.setHeaderText("Login Error");
                alert.setContentText(e.getMessage());
                alert.showAndWait();

            }
        });

    }

    private Optional<Pair<String, String>> login() {

        Dialog<Pair<String, String>> dialog = new Dialog<>();
        dialog.setTitle("Login Dialog");
        dialog.setHeaderText("Look, a Custom Login Dialog");

        // Set the button types.
        ButtonType loginButtonType = new ButtonType("Login", ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(loginButtonType, ButtonType.CANCEL);

        // Create the username and password labels and fields.
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

        TextField username = new TextField();
        username.setPromptText("Username");
        PasswordField password = new PasswordField();
        password.setPromptText("Password");

        grid.add(new Label("Username:"), 0, 0);
        grid.add(username, 1, 0);
        grid.add(new Label("Password:"), 0, 1);
        grid.add(password, 1, 1);

        // Enable/Disable login button depending on whether a username was entered.
        Node loginButton = dialog.getDialogPane().lookupButton(loginButtonType);
        loginButton.setDisable(true);

// Do some validation (using the Java 8 lambda syntax).
        password.textProperty().addListener((observable, oldValue, newValue) -> {
            loginButton.setDisable(newValue.trim().isEmpty());
        });

        dialog.getDialogPane().setContent(grid);
        // Request focus on the username field by default.
        Platform.runLater(() -> username.requestFocus());

// Convert the result to a username-password-pair when the login button is clicked.
        dialog.setResultConverter(dialogButton -> {
            if (dialogButton == loginButtonType) {
                return new Pair<>(username.getText(), password.getText());
            }
            return null;
        });

        Optional<Pair<String, String>> result = dialog.showAndWait();

        return result;

    }

}
