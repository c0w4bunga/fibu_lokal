package ui;

import buchung.Buchung;
import buchung.Buchungen;
import gruppe.Gruppen;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.stage.Stage;
import konto.Bestandskonto;
import konto.Konten;
import user.User;
import user.Users;

import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class FiBuController implements Initializable {

    private static Konten konten;
    private static Users users;
    private static Gruppen gruppen;
    private static Buchungen buchungen;

    @FXML private ListView<String> kontenHistory;
    @FXML private ComboBox<String> cbKonten;

    /**
     * Füllt die Combobox
     */
    private void fillCBKonten() {

        ObservableList<String> items = FXCollections.observableArrayList(
                this.konten.getKontenAsList());

        this.cbKonten.setItems(items);
    }

    /**
     * Öffnet das Buchungsfenster
     */
    public  void runBuchen() {
        Stage primaryStage = new Stage();

        try {
            Parent root = FXMLLoader.load(getClass().getResource("buchen/Buchen.fxml"));
            primaryStage.setTitle("Buchen");
            primaryStage.setScene(new Scene(root));
            primaryStage.setResizable(false);
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * Initialisiert Daten
     *
     * @param location
     * @param resources
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        try {
            buchungen = Buchungen.loadBuchungen("buchungen");
        } catch (Exception e) {
            buchungen = new Buchungen();
        }
        try {

            konten = Konten.loadKonten("konten");
        } catch (Exception e) {
            generateKonten();
        }

        try {
            users = Users.loadUsers("nutzer");
        } catch (Exception e) {
            users = new Users();
        }

        try {
            gruppen = Gruppen.loadGroups("gruppen");
        } catch (Exception e) {
            System.out.println("neue Gruppen");
            gruppen = new Gruppen();
        }


        fillCBKonten();
        cbKonten.setValue(cbKonten.getItems().get(0));
        refreshKontenHistroy(getHostory(cbKonten.getValue()));

        cbKonten.getSelectionModel().selectedItemProperty().addListener( new ChangeListener<String>()  {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {

                refreshKontenHistroy(getHostory(newValue));
            }
        });
    }

    /**
     * Öffnet das Nutzerfenster
     *
     */
    public void runUser() {

        Stage primaryStage = new Stage();

        try {
            Parent root = FXMLLoader.load(getClass().getResource("user/User.fxml"));
            primaryStage.setTitle("Nutzermanagement");
            primaryStage.setScene(new Scene(root));
            primaryStage.setResizable(false);
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Öffnet das Gruppenfenster
     *
     */
    public void runGroup() {

        Stage primaryStage = new Stage();

        try {
            Parent root = FXMLLoader.load(getClass().getResource("gruppe/Gruppe.fxml"));
            primaryStage.setTitle("Gruppenmanagement");
            primaryStage.setScene(new Scene(root));
            primaryStage.setResizable(false);
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public void runGruppenrechte() {

        Stage primaryStage = new Stage();

        try {
            Parent root = FXMLLoader.load(getClass().getResource("gruppe/rechte/Gruppenrechte.fxml"));
            primaryStage.setTitle("Gruppenrechte");
            primaryStage.setScene(new Scene(root));
            primaryStage.setResizable(false);
            primaryStage.show();
        } catch (Exception e) {
            System.out.println("nicht geladen");
            e.printStackTrace();
        }

    }

    /**
     * Aktualisiert die History
     *
     * @param list
     */
    private void refreshKontenHistroy(ArrayList<String> list) {

        ObservableList<String> items = FXCollections.observableArrayList(list);
        kontenHistory.setItems(items);

    }

    /**
     * Stellt die History zusammen und gibt sie zurück
     *
     * @param kontoname
     * @return ArrayList<String>
     */
    private ArrayList<String> getHostory(String kontoname) {
        ArrayList<String> history = new ArrayList<>();

        for (Buchung buchung : buchungen.getBuchungen(kontoname)) {

            buchung.getSollKonten().forEach((k,v) -> {
                String datum = Integer.toString(buchung.getDate().getTag())
                        + "." + Integer.toString(buchung.getDate().getMonat())
                        + "." + Integer.toString(buchung.getDate().getJahr());

                history.add(datum);
                history.add(buchung.getBuchungsNummer() + " " + k + " " + v);
            });
            buchung.getHabenKonten().forEach((k,v) -> {
                history.add(buchung.getBuchungsNummer() + " an " + k + " " + (v*-1));
            });

        }
        return history;
    }

    /**
     * Hier werden alle Konten erstellt
     *
     */
    private void generateKonten() {

        this.konten = new Konten();
        konten.addKonto(new Bestandskonto("Eigenkapital", 0.00,true));
        konten.addKonto(new Bestandskonto("Rohstoffe", 50000.00,false));
        konten.addKonto(new Bestandskonto("Immat. Verm. Gegenstände", 0.00,false));
        konten.addKonto(new Bestandskonto("Bank",0.00,false));
        konten.addKonto(new Bestandskonto("Kasse",0.00,false));
        konten.addKonto(new Bestandskonto("Verbindlichkeiten a.ll.", 0.00,true));
        konten.saveKonten("konten");


    }


    /**
     * Gibt den Container für Konten zurück
     *
     * @return Konten
     */
    public static Konten getKonten() {

        return konten;

    }

    /**
     * Gibt den Container für Buchungen zurück
     *
     * @return Buchungen
     */
    public static Buchungen getBuchungen() {
        return buchungen;
    }

    /**
     * Gibt den Container für Nutzer zurück
     *
     * @return Users
     */
    public static Users getUsers() {return users;}

    /**
     * Gibt den Container für Gruppen zurück
     *
     * @return Gruppen
     */
    public static Gruppen getGroups() {return gruppen;}


}
