package buchung;

import konto.Konten;
import ui.FiBuController;

import java.io.*;
import java.util.ArrayList;
import java.util.Map;

public class Buchungen implements Serializable {

    private ArrayList<Buchung> buchungen;

    /**
     * Erstellt eine leere Liste
     *
     */
    public Buchungen() {
        this.buchungen = new ArrayList<>();
    }

    /**
     * Führt eine Buchung aus
     *
     * @param sollKonten
     * @param habenKonten
     */
    public void addBuchung(Map<String, Double> sollKonten, Map<String, Double> habenKonten) {

        int buchungsNummer = buchungen.size() + 1;

        buchungen.add(new Buchung(buchungsNummer,sollKonten,habenKonten));

    }

    /**
     * Gibt die Buchung zurück
     * Wenn die Buchung nicht gefunden wird return = null
     *
     * @param buchungsnummer
     * @return Buchung
     */
    public Buchung getBuchung (int buchungsnummer) {

        for (Buchung b : buchungen) {

            if (b.getBuchungsNummer() == buchungsnummer)
                return b;

        }

        return null;

    }

    /**
     * Speichert alle Buchungen
     *
     * @param path
     */
    public void saveBuchungen (String path) {

        try {
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(new File(path)));
            out.writeObject(this);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * Ladet alle Buchungen
     *
     * @param path
     * @return
     * @throws Exception
     */
    public static Buchungen loadBuchungen(String path) throws Exception {

        ObjectInputStream in = new ObjectInputStream(new FileInputStream(new File(path)));
        return (Buchungen) in.readObject();

    }

    /**
     * Gibt alle Buchungen zurück, wo das entsprechende Konto enthalten ist
     *
     * @param kontoname
     *
     * @return ArrayList<Buchung>
     */
    public ArrayList<Buchung> getBuchungen (String kontoname) {

        ArrayList<Buchung> buchungList = new ArrayList<>();

        for (Buchung buchung : buchungen) {

            buchung.getSollKonten().forEach((k,v) -> {
                if(k.equals(kontoname))
                    buchungList.add(buchung);
            });

            buchung.getHabenKonten().forEach((k,v) -> {
                if(k.equals(kontoname))
                    buchungList.add(buchung);
            });

        }

        return buchungList;

    }
}
