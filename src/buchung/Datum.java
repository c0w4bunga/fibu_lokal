package buchung;

import java.io.Serializable;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class Datum implements  Comparable <Datum>, Serializable {

    private int tag;
    private int monat;
    private int jahr;

    /**
     * Erstellt ein Datum
     *
     * @param tag
     * @param monat
     * @param jahr
     */
    public Datum(int tag, int monat, int jahr) {

        this.tag   = tag;
        this.monat = monat;
        this.jahr  = jahr;

    }

    /**
     * Erstellt das aktuelle Datum
     *
     */
    public Datum() {

        GregorianCalendar cal = new GregorianCalendar();
        this.tag = cal.get(Calendar.DATE);
        this.monat = cal.get(Calendar.MONTH) + 1;
        this.jahr  = cal.get(Calendar.YEAR);

    }

    /**
     * Gibt den Tag zurück
     *
     * @return int
     */
    public int getTag() {
        return tag;
    }

    /**
     * Überschreibt den Tag
     *
     * @param tag
     */
    public void setTag(int tag) {
        this.tag = tag;
    }

    /**
     * Gibt den Monat zurück
     *
     * @return int
     */
    public int getMonat() {
        return monat;
    }

    /**
     * Überschreibt den Monat
     *
     * @param monat
     */
    public void setMonat(int monat) {
        this.monat = monat;
    }

    /**
     * Gibt das Jahr zurück
     *
     * @return int
     */
    public int getJahr() {
        return jahr;
    }

    /**
     * Überschreibt das Jahr
     *
     * @param jahr
     */
    public void setJahr(int jahr) {
        this.jahr = jahr;
    }


    /**
     * Vergleicht das Datum mit einem anderen
     *
     * @param o
     * @return int
     */
    @Override
    public int compareTo(Datum o) {

        if (this.jahr > o.getJahr()) {

            return 1;

        } else if (this.jahr < o.getJahr()) {

            return -1;

        } else if (this.monat > o.getMonat()) {

            return 1;

        } else if (this.monat < o.getMonat()) {

            return -1;

        } else if (this.tag > o.getTag()) {

            return 1;

        } else if (this.tag < o.getTag()) {

            return -1;

        }
        return 0;
    }
}
