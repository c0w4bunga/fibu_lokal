package buchung;

import konto.Konten;
import konto.Konto;
import ui.FiBuController;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Map;

public class Buchung implements Serializable {

    private int buchungsNummer;
    private Map<String, Double> sollKonten;
    private Map<String, Double> habenKonten;
    private Datum date;

    /**
     * Führt eine Buchung aus
     *
     * @param buchungsNummer
     * @param sollKonten
     * @param habenKonten
     */
    public Buchung(int buchungsNummer, Map<String, Double> sollKonten, Map<String, Double> habenKonten) {
        this.date           = new Datum();
        this.buchungsNummer = buchungsNummer;
        this.sollKonten     = sollKonten;
        this.habenKonten    = habenKonten;

        buchen();

    }

    /**
     * Führt die Buchung aus
     */
    private void buchen() {
        sollKonten.forEach((konto, value) -> {
            for (Konto okonto : FiBuController.getKonten().getKonten()){
                if (okonto.getName().equals(konto)) {
                    okonto.addWert();
                }
            }
        });

        habenKonten.forEach((konto, value) -> {
            for (Konto okonto : FiBuController.getKonten().getKonten()){
                if (okonto.getName().equals(konto)) {
                    okonto.addWert();
                }
            }
        });

    }

    /**
     * Gibt das Buchungsdatum zurück
     *
     * @return Datum
     */
    public Datum getDate() {
        return date;
    }

    /**
     * Gibt eine Map zurück welche die Sollkonten und deren wert enthält
     *
     * @return int
     */
    public int getBuchungsNummer() {
        return buchungsNummer;
    }

    /**
     * Gibt alle Sollkonten mit deren Wert zurück
     *
     * @return Map<String, Double>
     */
    public Map<String, Double> getSollKonten() {
        return sollKonten;
    }

    /**
     * Gibt eine Map zurück welche die Habenkonten und deren wert enthält
     *
     * @return
     */
    public Map<String, Double> getHabenKonten() {
        return habenKonten;
    }
}
