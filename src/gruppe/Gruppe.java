package gruppe;

import java.io.Serializable;

public class Gruppe implements Serializable {

    private int id;
    private String name;
    private GruppenRechte gruppenrechte;

    /**
     * Konstruktor um eine Gruppe zu erstellen
     *
     * @param id
     * @param name
     */
    public Gruppe(int id, String name) {
        this.id            = id;
        this.name          = name;
        this.gruppenrechte = new GruppenRechte();
    }

    /**
     * Gibt die Gruppen id zurück
     *
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     * Gibt den Benutzernamen zurück
     *
     * @return String
     */
    public String getName() {
        return name;
    }

    /**
     * ändert den Benutzernamen
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gibt die Gruppenrechte zurück
     *
     * @return GruppenRechte
     */
    public GruppenRechte getGruppenrechte() {
        return gruppenrechte;
    }

    /**
     * setzt die Gruppenrechte
     *
     * @param gruppenrechte
     */
    public void setGruppenrechte(GruppenRechte gruppenrechte) {
        this.gruppenrechte = gruppenrechte;
    }
}
