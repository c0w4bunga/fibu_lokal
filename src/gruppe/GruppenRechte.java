package gruppe;

import konto.Konten;

import java.io.Serializable;
import java.util.ArrayList;

public class GruppenRechte implements Serializable {

    private ArrayList<String> konten;
    private boolean kannZurueckbuchen;
    private boolean siehtUserManagement;
    private boolean kannAbschließen;


    /**
     * Initialisiert Daten
     */
    public GruppenRechte() {

        konten = new ArrayList<>();

    }
    /**
     * Gibt an ob die Gruppe Rechte für das Konto hat
     *
     * @param name
     * @return
     */
    public boolean rechtAufKonto (String name) {

        for (String konto : konten) {
            if (konto.equals(name)) return true;
        }

        return false;

    }

    /**
     * Gibt der Gruppe rechte über das Konto
     *
     * @param name
     */
    public void addKontoRecht (String name) {

        if (!rechtAufKonto(name)) konten.add(name);

    }

    /**
     * entfernt der Gruppe die Rechte über das Konto
     *
     * @param name
     */
    public void deleteKontoRechte (String name) {

        for (String konto: konten) {
            if (konto.equals(name)) konten.remove(konto);
        }

    }

    /**
     * Gibt an ob die Gruppe Buchungen zurück buchen kann
     *
     * @return boolean
     */
    public boolean isKannZurueckbuchen() {
        return kannZurueckbuchen;
    }

    /**
     * Gibt oder entnimmt der Gruppe das Recht zu stornieren
     *
     * @param kannZurueckbuchen
     */
    public void setKannZurueckbuchen(boolean kannZurueckbuchen) {
        this.kannZurueckbuchen = kannZurueckbuchen;
    }

    /**
     * Gibt an ob die Gruppe rechte über das Usermanagemant hat
     *
     * @return boolean
     */
    public boolean isSiehtUserManagement() {
        return siehtUserManagement;
    }

    /**
     * Gibt oder entnimmt der Gruppe die Rechte über das USermanagement
     *
     * @param siehtUserManagement
     */
    public void setSiehtUserManagement(boolean siehtUserManagement) {
        this.siehtUserManagement = siehtUserManagement;
    }

    /**
     * Gibt an ob die Gruppe das Geschäftsjahr abschließen kann
     *
     * @return boolean
     */
    public boolean isKannAbschließen() {
        return kannAbschließen;
    }

    /**
     * Gibt oder entnimmt der Gruppe das Recht um das Geschäftsjahr abzuschließen
     *
     * @param kannAbschließen
     */
    public void setKannAbschließen(boolean kannAbschließen) {
        this.kannAbschließen = kannAbschließen;
    }

    /**
     * Gibt die berechtigten Konten zurück als Liste
     *
     * @return ArrayList<String>
     */
    public ArrayList<String> getKonten() {
        return konten;
    }
}
