package gruppe;

import java.io.*;
import java.util.ArrayList;

public class Gruppen implements Serializable {

    private ArrayList<Gruppe> gruppen;


    /**
     * Konstruktor um eine leere Liste zu erstellen
     */
    public Gruppen() {
        gruppen = new ArrayList<>();
    }

    /**
     * Konstruktor um eine bereits vorhandene Liste zu nutzen
     *
     * @param gruppen
     */
    public Gruppen(ArrayList<Gruppe> gruppen) {
        this.gruppen = gruppen;
    }

    /**
     * Fügt eine neue Gruppe der Liste hinzu
     *
     * @param gruppe
     */
    public void addGruppe(Gruppe gruppe) {

        if (getGruppe(gruppe.getName()) == null) gruppen.add(gruppe);
    }

    /**
     * Speichert alle Gruppen
     *
     * @param path
     */
    public void saveGroups(String path) {

        try {
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(new File(path)));
            out.writeObject(this);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * Lädt eine Gruppenliste
     *
     * @param path
     * @return
     * @throws Exception
     */
    public static Gruppen loadGroups(String path) throws Exception {

        ObjectInputStream in = new ObjectInputStream(new FileInputStream(new File(path)));
        return (Gruppen) in.readObject();

    }

    /**
     * Löscht eine Gruppe
     *
     * @param name
     */
    public void deleteGruppe (String name) {

        gruppen.remove(getGruppe(name));

    }

    /**
     * Ändert den Namen einer Gruppe
     *
     * @param gruppenname
     * @param neuerName
     */
    public void rename (String gruppenname, String neuerName) {

        if(getGruppe(neuerName) == null) {
            getGruppe(gruppenname).setName(neuerName);
        }

    }

    /**
     * Gibt eine Gruppe zurück
     *
     * @param name
     * @return
     */
    public Gruppe getGruppe(String name) {

        for (Gruppe gruppe : gruppen) {

            if (gruppe.getName().equals(name)) return gruppe;
        }

        return null;

    }


    /**
     * Gibt die Gruppen zurück
     *
     * @return ArrayList<Gruppe>
     */
    public ArrayList<Gruppe> getGruppen() {
        return gruppen;
    }

    /**
     * Gibt eine Gruppenliste zurück
     *
     * @return ArrayList<String>
     */
    public ArrayList<String> getGroupsAsList() {

        ArrayList<String> groupList = new ArrayList<>();

        for (Gruppe group : gruppen) {

            groupList.add(group.getName());

        }

        return groupList;

    }

}
