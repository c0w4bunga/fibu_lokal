package konto;

import java.io.Serializable;
import java.util.ArrayList;

public abstract class Konto implements Serializable {

    private String name;
    private ArrayList<Double> werte;
    private double currentBuchungswert;

    /**
     * Erstellt ein leeres Konto
     * @param name
     */
    public Konto(String name) {
        this.name = name;
        werte     = new ArrayList<>();
    }

    /**
     * Fügt der Liste den aktuellen Buchungswert hinzu
     *
     */
    public void addWert() {
        werte.add(currentBuchungswert);
    }

    /**
     * Schließt das Konto ab
     */
    public double abschließen() {

        double saldo = 0.0;

        for (double wert :werte) {

            saldo += wert;

        }

        return  saldo;

    }

    /**
     * Gibt den Namen des Kontos zurück
     *
     * @return String
     */
    public String getName() {
        return name;
    }

    /**
     * Liefert den aktuellen Buchungswert zurück
     *
     * @return double
     */
    public double getCurrentBuchungswert() {return currentBuchungswert;}

    /**
     * Setzt den aktuellen Buchungswert
     *
     * @param currentBuchungswert
     */
    public void setCurrentBuchungswert(double currentBuchungswert) {
        this.currentBuchungswert = currentBuchungswert;
    }
}
