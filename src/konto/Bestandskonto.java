package konto;

public class Bestandskonto extends Konto {

    private double anfansbestand;
    private boolean isPassive;

    /**
     * Erstellt ein Bestandskonto
     *
     * @param name
     * @param anfansbestand
     * @param isPassive
     */
    public Bestandskonto(String name, double anfansbestand, boolean isPassive) {
        super(name);
        this.anfansbestand = anfansbestand;
        this.isPassive     = isPassive;
    }


    /**
     * Gibt an ob es Ein Passivkonto ist
     *
     * @return boolean
     */
    public boolean isPassive() {
        return isPassive;
    }

    /**
     * Gibt den Anfangsbestand zurück
     *
     * @return double
     */
    public double getAnfansbestand() {
        return anfansbestand;
    }
}
