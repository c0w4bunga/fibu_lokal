package user;

import java.io.*;
import java.util.ArrayList;

public class Users implements Serializable {

    private ArrayList<User> users;

    /**
     * Konstruktor erzeugt ein leeres Objekt
     */
    public Users() {
        users = new ArrayList<>();
    }

    /**
     * Fügt einen neuen Nutzer der Nutzerliste hinzu
     *
     * @param user
     */
    public void addUser(User user){

        users.add(user);

    }

    /**
     * Weißt einem Benutzer eine Gruppe zu
     *
     * @param loginname
     * @param id
     */
    public void addGruppe(String loginname, int id){

        getUser(loginname).addGruppe(id);

    }

    /**
     * Speichert alle Nutzer
     *
     * @param path
     */
    public void saveUsers (String path) {

        try {
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(new File(path)));
            out.writeObject(this);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * Lädt alle Nutzer
     *
     * @param path
     * @return
     * @throws Exception
     */
    public static Users loadUsers(String path) throws Exception {

        ObjectInputStream in = new ObjectInputStream(new FileInputStream(new File(path)));
        return (Users) in.readObject();

    }

    /**
     * Löscht einen Nutzer
     *
     * @param username
     */
    public void deleteUser(String username) {

        users.remove(getUser(username));

    }

    /**
     * Ändert den Benutzernamen
     *
     * @param loginname
     * @param username
     */
    public void rename(String loginname, String username) {

        getUser(loginname).setName(username);

    }

    /**
     * Gibt die Nutzer zurück
     *
     * @return ArrayList<User>
     */
    public ArrayList<User> getUsers() {
        return users;
    }

    /**
     * Gibt eine Nutzerliste zurück
     *
     * @return ArrayList<String>
     */
    public ArrayList<String> getUsersAsList() {

        ArrayList<String> nutzerListe = new ArrayList<>();

        for (User user : users) {
            nutzerListe.add(user.getName());
        }

        return nutzerListe;
    }

    /**
     * Gibt einen Nutzer zurück
     *
     * @param username
     * @return User
     */
    public User getUser(String username) {

        for (User user : users) {
            if (user.getName().equals(username)) return user;
        }

        return null;
    }

    /**
     * Beim Einloggen wird der Loginname und das Passwort überprüft
     *
     * @param loginname
     * @param password
     * @return boolean
     */
    public boolean validateUser(String loginname, String password) {

        for (User user : users) {
            if (user.getLoginname().equals(loginname) && user.getPassword().equals(password))
                return true;
        }

        return false;

    }
}
